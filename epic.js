//number 1
"use strict"
!(function () {
const imgTypes=['natural', 'enhanced', 'aerosol', 'cloud'];
    let EpicApplication={
        emptyarray:[],
        typeValue:null,
        datecache:{},
        imageCaches:{}
    };
    for(let i=0;i<imgTypes.length;i++){
        EpicApplication.datecache[imgTypes[i]]=null;
    }
    for (let i = 0; i < imgTypes.length; i++) {
        EpicApplication.imageCaches[imgTypes[i]] = new Map();
     }
    document.addEventListener("DOMContentLoaded", function (e) {
        
        const theimage = document.querySelector("#earth-image");
        const thetemplate = document.getElementsByTagName("template");
        const imagemenu = document.querySelector("#image-menu");
        const theform = document.querySelector("form");
        const thedate = document.getElementById("date");
        const thetype = document.getElementById("type");
        for(let i=0;i<imgTypes.length;i++){
            let theoption=document.createElement('option');
            theoption.value= imgTypes[i];
            theoption.textContent= imgTypes[i];
            thetype.appendChild(theoption);
        }
        
        theform.addEventListener("submit",function (e){
            e.preventDefault();
            imagemenu.textContent = undefined;
            EpicApplication.emptyarray=[];
            EpicApplication.typeValue= thetype.value; 
            
            
            if (EpicApplication.datecache[EpicApplication.typeValue]) {
                // If the date is cached, use it and do not make a fetch request
                thedate.value = EpicApplication.datecache[EpicApplication.typeValue];
                let dateKey=thedate.value;
                let cachedImg = EpicApplication.imageCaches[EpicApplication.typeValue].get(dateKey);
                if(cachedImg){
                    cachedImg.forEach(function(image) {
                    let li = document.createElement('li');
                    li.textContent = `Image Date: ${image.date}`; // Adjust to your image object's structure
                        imagemenu.appendChild(li);
                    });
                
            } else {
            fetch(`https://epic.gsfc.nasa.gov/api/${EpicApplication.typeValue}/date/${thedate.value}`,{})
            .then(response=>{
                //make sure its ok
                console.log( "Response object:", response);

                if(!response.ok){
                    throw new Error("Not 2xx response", { cause: response });
                }
                return response.json();
            }).then((hasani) =>{
                if(hasani===0){
                    noimgerror();
                    return;
                }
               
                EpicApplication.datecache[EpicApplication.typeValue]=hasani[0].date;
            for(let i =0;i<hasani.length;i++){
                const template = thetemplate.content.cloneNode(true);
                const li = template.querySelector('li');
                li.child[0].textContent = hasani[i].date;
                li.child[0].setAttribute('attribute',i);
                imagemenu.appendChild("li");
                
            }
            EpicApplication.emptyarray.push(hasani);
            }).catch((err) =>{
                console.log(err);
            });
        }else{
            fetch(`https://epic.gsfc.nasa.gov/api/${EpicApplication.typeValue}/date/${thedate.value}`,{})
            .then(response=>{
                //make sure its ok
                console.log( "Response object:", response);

                if(!response.ok){
                    throw new Error("Not 2xx response", { cause: response });
                }
                return response.json();
        })
        .then(dates=>{
            let lates=dates[0].date;
            EpicApplication.datecache[EpicApplication.typeValue] = latestDate;
            thedate.value=latestDate;
            fetch(`https://epic.gsfc.nasa.gov/api/${EpicApplication.typeValue}/date/${latestDate}`, {})
            .then(response => {
                if (!response.ok) {
                    throw new Error("Not 2xx response", { cause: response });
                }
                return response.json();
            })
            .then(images => {
                if (images.length === 0) {
                    noimgerror();
                    return;
                }
                EpicApplication.imageCaches[EpicApplication.typeValue].set(latestDate, images);
                images.forEach(function(image) {
                    let theli = document.createElement('li');
                    theli.textContent = `Image Date: ${image.date}`; 
                    imagemenu.appendChild(li);
        });
        })
        .catch(function(err) {
            console.error(err);
        });
})
.catch(function(err) {
    console.error( err);
});
        });
        thetype.addEventListener("change",(e)=>{
            max(thetype);
        });
        imagemenu.addEventListener("click",function(e){
            showimg(e, thedate, theimage, thetype);
        });


        function noimgerror() {
            
            const errclone = thetemplate.content.cloneNode(true);
            const listitem = errclone.querySelector('li');
            listitem.textContent = "img  not available.";
           
            imagemenu.appendChild(errclone);
        }



       
    });
}());

function showimg(e, thedate, theimage, thetype) {
    let index = e.target.getAttribute("attribue");
    let pic = EpicApplication.emptyarray[0][index].image;
    let date = thedate.value.split("-");
    let year = date[0];
    let month = date[1];
    let day = date[2];
    theimage.src = `https://epic.gsfc.nasa.gov/archive/${EpicApplication.typeValue}/${year}/${month}/${day}/jpg/${pic}.jpg`;
}

function max(thetype) {
    let typev=thetype.value;
    if(EpicApplication.datecache[typeValue]){
        thedate.value=EpicApplication.datecache[typev];
    }else{
    fetch(`https://epic.gsfc.nasa.gov/api/${EpicApplication.typeValue}/all`, {})
                .then(response => {

                    if (!response.ok) {
                        throw new Error("Not 2xx response", { cause: response });
                    }
                    return response.json();
                })
                .then(dates =>{
                    EpicApplication.datecache[typev]=dates[0].date;
                    thedate.value=dates[0].date;
                })
                .catch(err => {
                    console.error("3)Error:", err);
                });
            }        
}
